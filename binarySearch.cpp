/*Binary Search algorithm based on the idea to compare the target value to the middle element of the array.

If the target value is equal to the middle element - we're done.

If the target value is smaller - continue to search on the left.

If the target value is larger - continue to search on the right.*/

#include <vector>
#include <iostream>
using namespace std;

class Solution {
public:
    int search(vector<int>& nums, int target) {
        int retVal, end, begin;

        sort(nums.begin(),nums.end());

        int middle;
        int low=0;
        int high = nums.size()-1;

while(low<high){
    middle=low+(high-low)/2;
     
          if(target == nums[middle])
        {
            return middle;
        }
         else if(target <nums[middle])
        {
            //search from begin to middle
            high=middle-1;

        }
        else 
        {
            //adjust begin .search from middle to end
            low=middle;

        }
       
        
}


return -1;

      //  return retVal;
}
};


int main()
{
    Solution obj;
    vector<int> vec;
    vec.push_back(-1);
    vec.push_back(0);
    vec.push_back(3);
    vec.push_back(5);
    vec.push_back(9);
    vec.push_back(12);
    int num=9;
    int result=obj.search(vec, num);
    cout<<result<<endl;
    cin.get();
    return 0;
}