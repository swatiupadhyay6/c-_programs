#include <iostream>

using namespace std;
/*usage of default and delete operator*/
/*The classes where we can use compiler default versions should use default keyword. 
For eg, In the below class , I want to create copies of the object only.
In C++03 , If any of the construcotrs is defined, I would have needed to define the default constructors as well
but C++11 allows us to keep the customized version of some constructors while keeping compiler default
versions for othere. Use the keyword "default" to achieve that.
Also read the notes added in the end of this program*/


class Practice_default
{
    private:
            int data{10};
    public:
            Practice_default(const Practice_default &obj)
            {
                
                data=obj.data;
               
            }
            Practice_default()=default;

//use delete to make a function non-usable or non-accessable
//display with a string as param cannot be used by the user.
//display with zero params can be called and used.
            void display(string name)=delete;
           void display();
};



void Practice_default::display()
{
    cout<<"display function"<<endl;
}


int main()
{
  Practice_default obj;
  Practice_default obj1(obj);
  obj.display();
  obj1.display("swati");
  return 0;
    
    
}



/*Explicitly defaulted and deleted special member functions

In C++03, the compiler provides, for classes that do not provide them for themselves, a default constructor, a copy constructor, a copy assignment operator (operator=), and a destructor. The programmer can override these defaults by defining custom versions. C++ also defines several global operators (such as operator new) that work on all classes, which the programmer can override.
However, there is very little control over creating these defaults. Making a class inherently non-copyable, for example, requires declaring a private copy constructor and copy assignment operator and not defining them. Attempting to use these functions is a violation of the One Definition Rule (ODR). While a diagnostic message is not required,[16] violations may result in a linker error.
In the case of the default constructor, the compiler will not generate a default constructor if a class is defined with any constructors. This is useful in many cases, but it is also useful to be able to have both specialized constructors and the compiler-generated default.
C++11 allows the explicit defaulting and deleting of these special member functions.[17] For example, this type explicitly declares that it is using the default constructor:

struct SomeType
{
    SomeType() = default; //The default constructor is explicitly stated.
    SomeType(OtherType value);
};


*/



//https://bitbucket.org/swatiupadhyay6/c-_programs/src/master/practice_default_and_delete.cpps
