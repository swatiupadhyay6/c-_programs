//Approach 1 - traverse through the linked list and get the elements into a string.
//iterate from last element of the string to the first element of the string multiplying with pow(2, num) where num will increase
//along with the loop.
//Currently output is not correct for some reason.I am yet to figure out the issue.

/**
 * Definition for singly-linked list.
 * struct ListNode {
 *     int val;
 *     ListNode *next;
 *     ListNode() : val(0), next(nullptr) {}
 *     ListNode(int x) : val(x), next(nullptr) {}
 *     ListNode(int x, ListNode *next) : val(x), next(next) {}
 * };
 */


class Solution {
public:
    int getDecimalValue(ListNode* head) {
        int num=0, result=0;
        
        
        string str;
        
        while(head != nullptr)
        {
            cout<<head->val<<endl;
            str = str+to_string(head->val);
            head=head->next;
            
            
        }
       for(int loop=str.size()-1;loop>=0;loop--)
            {
               result=str[loop]*(pow(2,num));
             //   cout<<"result: "<<str[loop]<<endl;
                num++;
            }
    
        
        return result;   }
};


/* Approach2 - iterate through the list and keep multiplying 2 with the sum. For eg.
for binary number 1011, do below:
2*sum+digit
2*0+1 = 1
2*1+0= 2
2*2+1=5
2*5+1 = 11
*/

int getDecimalValue(ListNode* head) {
        int num=0, result=0;
        
        
       while(head != nullptr)
       {
           result=result*2+head->val;
           head=head->next;
       }
        return result;
}


/*Approach 3 - Using bitwise operator
*/

/*approach 4 - using recursion*/