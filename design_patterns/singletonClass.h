#ifndef singletonClass
#define singletonClass
class Singleton
{
    private: Singleton()
    {
       
    }

    public: static Singleton* getInstance();
            static Singleton *dbconn;

            /*singletons should not be clonnable. No copy constructor should be used,  makes sense*/
            Singleton(const Singleton &obj)=delete;

            /*singletons should not be assignable. makes sense */

            void operator=(const Singleton &obj)=delete;

            /*do I need to really define a  destructor . I dont think so?*/

            Singleton::~Singleton()
            {

            }

};
#endif