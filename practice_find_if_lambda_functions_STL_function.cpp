#include <iostream>
#include <array>
#include <algorithm>
#include <string_view> //use string_view header
using namespace std;


/*if you are passing string_view param to function, recive the param in a string_view argument only*/
/* ie. str in the below function declaration should be string_view type*/
/*bool containsNut(string_view str)
{
    return (str.find("nut")!=string_view::npos);
    
}*/

int main()
{

    array<string_view, 4> arr{"apple", "banana", "walnut", "lemon"};
    auto found{find_if(arr.begin(), arr.end(), [](string_view str){{
    return (str.find("nut")!=string_view::npos);
    
}})};
    if(found == arr.end())
    {
        cout<<"no Nuts"<<endl;
    }
    else{
        cout<<"Nuts Found"<<endl;
    }
    return 0;
}