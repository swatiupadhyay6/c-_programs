 /*Approach 1 - use a set to remove the elements from the list*/
 
 class Solution {
public:
    ListNode* deleteDuplicates(ListNode* head) {
 set<int> mySet;
        ListNode *prev=nullptr;
        ListNode *curr=head;
        while(curr != nullptr)
        {
            if(find(mySet.begin(), mySet.end(), head->val) == mySet.end()){
                mySet.insert(curr->val);
                prev=curr;
                
            }
            else
            {
                prev->next=curr->next;
                delete curr;
            }
            
            curr=prev->next;
        }
        return head;
    }