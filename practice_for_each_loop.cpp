#include <iostream>
using namespace std;
int main()
{

 /*constexpr int scores[]{84,78,95};
 constexpr int numStudents{size(scores)};


int maxScore{0};

//simple for loop
for(int student{0};student<numStudents;student++)
{
        if(scores[student] > maxScore)
        {
            maxScore=scores[student];
        }
}*/


//constexpr keyword cannot be used with string. Why? 
//Use string_view instead.
//constexpr string str{"swati"};


//below will work fine. Use string_view to use constexpr with a string.

 string_view str[]{"one", "two", "three", "four", "five"};

//below str is iterated by value

for(auto element: str)
{
    cout<<element<<endl;
}



string str2[]{"peter", "likes", "frozen", "yoghurt"};

//below string is iterated by reference
//In the below example, element will be a reference to the currently iterated array element, avoiding having to make a copy. Also any changes to element will affect the array being iterated over,
for(auto &element: str2)
{
    cout<<element<<endl;
}

    return 0;
}
