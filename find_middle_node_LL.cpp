/**
 * Definition for singly-linked list.
 * struct ListNode {
 *     int val;
 *     ListNode *next;
 *     ListNode() : val(0), next(nullptr) {}
 *     ListNode(int x) : val(x), next(nullptr) {}
 *     ListNode(int x, ListNode *next) : val(x), next(next) {}
 * };
 */



/*Approach 1 - 
Use a vector to insert list values to it and keep adding until the last element ->next == nullptr.

*/

class Solution {
public:
    ListNode* middleNode(ListNode* head) {
        
        vector<ListNode*> A={head};
        while(A.back()->next != nullptr)
        {
            A.push_back(A.back()->next);
        }
        return A[A.size()/2];

    }
};





/* Approach 2 - Fast and slow pointer */


class Solution {
public:
    ListNode* middleNode(ListNode* head) {
      if(head == nullptr || head->next== nullptr)
     {
         return head;
     }
ListNode *slow=head, *fast=head;
     while(fast != nullptr && fast ->next !=nullptr)
     {
        fast=fast->next->next;
        slow=slow->next;
     }   

     return slow;

    }
};