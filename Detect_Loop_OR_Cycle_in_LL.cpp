/**
 * Definition for singly-linked list.
 * struct ListNode {
 *     int val;
 *     ListNode *next;
 *     ListNode(int x) : val(x), next(NULL) {}
 * };
 */
class Solution {
public:
    bool hasCycle(ListNode *head) {
        ListNode *slow=head, *fast=head;
        if(head == nullptr||head->next== nullptr)
        {
            return false;
        }
        while(fast != nullptr && fast->next != nullptr)
        {
            
            fast=fast->next->next;
            slow=slow->next;
            if(slow == fast)
            {
                return true;
            }
        }
        return false;
        
        
    }

    
    
};


/*Main cases to consider are:

1- if the linked list is empty
2 - if the linked list has 1 node only. such a LL cant have a cycle.
3 - if the LL does not contain a loop , do not try to access a nullpte location - check 2nd condition of while above
*/